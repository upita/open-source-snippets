### utility-stringhasher
This is a collection of simple and fast -but unsafe- hash functions.
We use these functions to hash GameObject's identifying string, so that type comparison is 10 times faster then a simple std::string comparison.

### WARNING
You should't use these functions in a context where security is important, they are meant for fast string hashing in videogames.
