#include "stringhasher.h"
#include <iostream>
#include <ctime>
#include <fstream>

int main(int argc, char const *argv[]) {

	std::string s;

	std::ifstream stream;
	stream.open("checksum.txt");
	std::string line;
	if(stream.is_open()) {
		while(std::getline(stream, line)) {
			s += line;
		}
	}


	std::cout << checksum_mi(s) << std::endl;

	return 0;
}
