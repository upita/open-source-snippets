#ifndef _STRINGHASHER_H
#define _STRINGHASHER_H
#include <string>

constexpr int primes[32] = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 
							73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131 };

constexpr int noise[32] = { 1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 7, 7, 7,
							7, 7, 7, 8, 8, 8, 8, 8 };


inline unsigned long int hash_mi(std::string s) {
	register unsigned long int result = 0;
	const size_t size = s.size();
	if(size > 32) return 0;

	for(size_t i = 0; i < size; ++i)
		result += (s[i] * primes[i]) + ((result >> 2) * noise[i]);

	return result;
}

inline unsigned long int hash_mi2(std::string s) {
	register unsigned long int result = 0;
	const size_t size = s.size();
	if(size > 32) return 0;

	for(size_t i = 0; i < size; ++i)
		result += (s[i] * primes[i]) << noise[i];
	return result;
}

inline unsigned long int hash_elf(std::string s) {
	register unsigned long int result = 0;
	register unsigned long int coeff = 0;

	const size_t size = s.size();
	if(size > 32) return 0;

	for(size_t i = 0; i < size; ++i) {
		result = (result << 4) + s[i];
		coeff = result & 0xf0000000l;
		if(coeff != 0)
			result ^= (coeff >> 24);
		result &= ~coeff;
	}
	return result;

	return 0;
}

inline unsigned long int hash_js(std::string s) {
	register unsigned int result = 1315423911;

	for(std::size_t i = 0; i < s.length(); i++)
		result ^= ((result << 5) + s[i] + (result >> 2));

	return result;
}

inline unsigned long int checksum_simple(std::string s) {
	register unsigned int result = 0;
	const size_t size = s.size();
	for(size_t i = 0; i < size; ++i) {
		result += int(s[i]);
	}
	return result;
}

inline unsigned long long checksum_mi(std::string s) {
	register unsigned long result = 0;
	const size_t size = s.size();
	size_t noise_coeff = 0;
	for(size_t i = 0; i < size; ++i) {
		result += int(s[i]) * noise[noise_coeff];
		noise_coeff++;
		if(noise_coeff > 32) noise_coeff = 0;
	}
	return result;
}

inline unsigned long long checksum(std::string s) {
	return checksum_mi(s);
}

inline unsigned long int hash(std::string s) {
	return hash_mi(s);
}


#endif
