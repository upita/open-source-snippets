#include "memorypool.h"
#include <iostream>

/*
*Example use
*/

struct MyClass {
	int integer;
};

int main(int argc, char const *argv[]) {
	
	MemoryPool<MyClass, 128> mempool;

	AllocatedBlock<MyClass> block = mempool.alloc();
	
	MyClass* ptr = block.ptr;
	ptr->integer = 3;

	std::cout << mempool.getfreemem() << std::endl;

	mempool.free(block);

	return 0;
}
