#ifndef _MEMORYPOOL_H
#define _MEMORYPOOL_H
#include <queue>

template<typename T>
class AllocatedBlock {
	public:
		AllocatedBlock(unsigned int index, T* ptr) {
			this->index = index;
			this->ptr = ptr;
		}

		AllocatedBlock() {}

		unsigned int index;
		T* ptr;
};

template<typename T, unsigned int poolsize>
class MemoryPool {
	private:
		T pool[poolsize];
		std::queue<unsigned int> freeblocks;
	public:
		AllocatedBlock<T> mainblock;


		MemoryPool() {
			mainblock = AllocatedBlock<T>(0, &pool[0]);
		}

		~MemoryPool() {}

		AllocatedBlock<T> alloc() {

			if(mainblock.index < poolsize - 1) {
				mainblock.index++;
				return AllocatedBlock<T>(mainblock.index, &pool[mainblock.index]);
			} else if(freeblocks.size() > 0) {
				AllocatedBlock<T> ab = AllocatedBlock<T>();
				ab.index = freeblocks.front();
				ab.ptr = &pool[freeblocks.front()];
				freeblocks.pop();
				return ab;
			} else {
				return AllocatedBlock<T>(0, nullptr);
			}
			
		}

		void free(AllocatedBlock<T>& ab) {
			freeblocks.push(ab.index);
			ab.ptr = nullptr;
		}

		unsigned int getfreemem() { return (freeblocks.size() - 1) + (poolsize - mainblock.index); }
	
		void clear() {
			for(unsigned int i = 0; i < poolsize; ++i) pool[i] = 0;
			for(unsigned int i = 0; i < freeblocks.size(); ++i) freeblocks.pop();
		}

};

#endif
