#ifndef _PSEUDORANDOM_H
#define _PSEUDORANDOM_H

namespace upita {

	class PseudoRandom {
		private:
			unsigned long gen;
			unsigned long seed;
			unsigned long max;
		public:
			inline PseudoRandom() {}
			inline PseudoRandom(unsigned long seed, unsigned long gen, unsigned long max) : 
			seed(seed), gen(gen), max(max) {}
			inline ~PseudoRandom() {}

			inline unsigned long get() {
				seed = ((gen * seed) + (gen * 2)) % max;
				return seed;
			}

			inline void setSeed(unsigned long seed) { this->seed = seed; }

			inline void setGen(unsigned long gen) { this->gen = gen; }

			inline void setMax(unsigned long max) { this->max = max; }
			
	};

}

#endif
