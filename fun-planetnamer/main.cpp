#include "namegen.h"
#include <iostream>

int main(int argc, char const *argv[]) {
	NameGenerator::create();
	std::cout << NameGenerator::get(4) << std::endl;
	return 0;
}
