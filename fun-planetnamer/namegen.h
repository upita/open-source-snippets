#ifndef _NAMEGEN_H
#define _NAMEGEN_H
#include <string>
#include <vector>
#include "pseudorandom.h"

class NameGenerator {
	private:
		static upita::PseudoRandom rand;
		static std::vector<std::string> table;
	public:
		NameGenerator() = delete;
		~NameGenerator() = delete;
		
		static void create();

		static std::string get();

		static std::string get(size_t num);

};

#endif
