#include "namegen.h"
#include "pseudorandom.h"
#include "ctime"

upita::PseudoRandom NameGenerator::rand {};
std::vector<std::string> NameGenerator::table;

void NameGenerator::create() {

	rand.setSeed((unsigned int) time(nullptr));
	rand.setGen((unsigned int) (time(nullptr)) + 1);
	rand.setMax(28 * 5);

	for (int i = 0; i < 28; ++i) {

		constexpr char v[6] = {'a', 'e', 'i', 'o', 'u', 'y'};

		for (int l = 0; l < 6; ++l) {
			std::string c {};
			c += 'a' + i;
			c += v[l];
			table.push_back(c);
		}
	}
}

std::string NameGenerator::get() {
	for(int i = 0; i < rand.get(); i++) rand.get();
	return table[rand.get()];
}

std::string NameGenerator::get(size_t num) {
	std::string res {};
	for (int i = 0; i < num; ++i) res += table[rand.get()];
	return res;
}
