#ifndef _PSEUDORANDOM_H
#define _PSEUDORANDOM_H

namespace upita {

	class PseudoRandomizer {
		private:
			static unsigned long gen;
			static unsigned long seed;
			static unsigned long max;
		public:
			PseudoRandomizer() = delete;
			~PseudoRandomizer() = delete;

			static unsigned long get() {
				seed = ((gen * seed) + (gen * 2)) % max;
				return seed;
			}

			inline static void setSeed(unsigned long seed) { PseudoRandomizer::seed = seed; }

			inline static void setGen(unsigned long gen) { PseudoRandomizer::gen = gen; }

			inline static void setMax(unsigned long max) { PseudoRandomizer::max = max; }
			
	};

	class PseudoRandom {
		private:
			unsigned long gen;
			unsigned long seed;
			unsigned long max;
		public:
			inline PseudoRandom() {}
			inline PseudoRandom(unsigned long seed, unsigned long gen, unsigned long max) : 
			seed(seed), gen(gen), max(max) {}
			inline ~PseudoRandom() {}

			inline unsigned long get() {
				seed = ((gen * seed) + (gen * 2)) % max;
				return seed;
			}

			inline void setSeed(unsigned long seed) { this->seed = seed; }

			inline void setGen(unsigned long gen) { this->gen = gen; }

			inline void setMax(unsigned long max) { this->max = max; }
			
	};

}

unsigned long upita::PseudoRandomizer::gen = 0;
unsigned long upita::PseudoRandomizer::seed = 0;
unsigned long upita::PseudoRandomizer::max = 0;

#endif
