#ifndef _FASTSTRUCT_H
#define _FASTSTRUCT_H

template<typename T1>
class FastStruct1 {
	public:
		T1 t1;
};

template<typename T1, typename T2>
class FastStruct2 {
	public:
		T1 t1;
		T2 t2;
};

template<typename T1, typename T2, typename T3>
class FastStruct3 {
	public:
		T1 t1;
		T2 t2;
		T3 t3;
};

template<typename T1, typename T2, typename T3, typename T4>
class FastStruct4 {
	public:
		T1 t1;
		T2 t2;
		T3 t3;
		T4 t4;
};

template<typename T1, typename T2, typename T3, typename T4, typename T5>
class FastStruct5 {
	public:
		T1 t1;
		T2 t2;
		T3 t3;
		T4 t4;
		T5 t5;
};

template<typename T1, typename T2, typename T3, typename T4, typename T5,
		typename T6>
class FastStruct6 {
	public:
		T1 t1;
		T2 t2;
		T3 t3;
		T4 t4;
		T5 t5;
		T6 t6;
};

template<typename T1, typename T2, typename T3, typename T4, typename T5,
		typename T6, typename T7>
class FastStruct7 {
	public:
		T1 t1;
		T2 t2;
		T3 t3;
		T4 t4;
		T5 t5;
		T6 t6;
		T7 t7;
};

template<typename T1, typename T2, typename T3, typename T4, typename T5,
		typename T6, typename T7, typename T8>
class FastStruct8 {
	public:
		T1 t1;
		T2 t2;
		T3 t3;
		T4 t4;
		T5 t5;
		T6 t6;
		T7 t7;
		T8 t8;
};

template<typename T1, typename T2, typename T3, typename T4, typename T5,
		typename T6, typename T7, typename T8, typename T9>
class FastStruct9 {
	public:
		T1 t1;
		T2 t2;
		T3 t3;
		T4 t4;
		T5 t5;
		T6 t6;
		T7 t7;
		T8 t8;
		T9 t9;
};

template<typename T1, typename T2, typename T3, typename T4, typename T5,
		typename T6, typename T7, typename T8, typename T9, typename T10>
class FastStruct10 {
	public:
		T1 t1;
		T2 t2;
		T3 t3;
		T4 t4;
		T5 t5;
		T6 t6;
		T7 t7;
		T8 t8;
		T9 t9;
		T10 t10;
};

#endif
