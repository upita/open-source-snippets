#ifndef _FSM_H
#define _FSM_H
#include <map>
#include "fsmbase.h"

/**
*Finite state machine simple implementation.
*/
template<typename P>
class FSM {
	private:
		std::map<int, FSMBase<P>*> state_funcs;
		int current;
	public:

		FSM();

		~FSM();

		inline void add(FSMBase<P>* state, int event) {
			state_funcs.insert(std::pair<int, FSMBase<P>*>(state, event));
		}

		inline void remove(int event) {
			state_funcs.erase(event);
		}

		inline void update() {
			state_funcs[current]->update();
		}

		inline void enter(int state) {
			current = state;
			state_funcs[current]->enter();
		}
		
};

#endif
