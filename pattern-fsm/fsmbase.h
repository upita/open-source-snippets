#ifndef _FSMBASE_H
#define _FSMBASE_H

template<typename P>
class FSMBase {
	public:
		FSMBase();
		~FSMBase();

		virtual void update(P param) = 0;

		virtual void enter(P param) = 0;
		
};

template<>
class FSMBase<void> {
	public:
		FSMBase();
		~FSMBase();

		virtual void update() = 0;

		virtual void enter() = 0;
		
};

#endif
