#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath>

int main(int argc, char const *argv[]) {
	
	std::string stringbuff = "number";
	std::string stringbuffneg = "1";
	std::ofstream outfile;
	outfile.open("fact_LUT.h");
	if(!outfile) {
		std::cout << "Can't open file" << std::endl;
		return 0;
	}

	outfile << "switch(power) {" << std::endl;
	outfile << "	case 0: return 1; break;" << std::endl;
	outfile << "	case 1: return 1; break;" << std::endl;

	for (int i = 2; i < 31; ++i) {
		std::stringstream ss;
		ss << i;
		std::string buf = ss.str();

		long long numbuff = 1;
		for(int j = 1; j < (i + 1); ++j) {
			numbuff *= j;
		}

		std::stringstream ss2;
		ss2 << numbuff;
		std::string buf2 = ss2.str();

		stringbuff += " * number";
		outfile << ("	case " + std::string(buf) + ": return "
					+ buf2 + "; break;") << std::endl;
	}
	stringbuff = "1";

	outfile << "	default: return 0/0; break;" << std::endl;

	outfile << "}" << std::endl;


	return 0;
}
