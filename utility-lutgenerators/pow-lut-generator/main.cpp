#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

int main(int argc, char const *argv[]) {
	
	std::string stringbuff = "number";
	std::string stringbuffneg = "1";
	std::ofstream outfile;
	outfile.open("powLUT.h");
	if(!outfile) {
		std::cout << "Can't open file" << std::endl;
		return 0;
	}

	outfile << "switch(power) {" << std::endl;
	outfile << "	case 0: return 1; break;" << std::endl;
	outfile << "	case 1: return number; break;" << std::endl;

	for (int i = 2; i < 31; ++i) {
		std::stringstream ss;
		ss << i;
		std::string buf = ss.str();

		stringbuff += " * number";
		outfile << ("	case " + std::string(buf) + ": return "
					+ stringbuff + "; break;") << std::endl;
	}
	stringbuff = "1";

	for (int i = 1; i < 31; ++i) {
		std::stringstream ss;
		ss << -i;
		std::string buf = ss.str();

		stringbuff += " / number";
		outfile << ("	case " + std::string(buf) + ": return "
					+ stringbuff + "; break;") << std::endl;
	}	

	outfile << "	default: return 0 / 0; break;" << std::endl;

	outfile << "}" << std::endl;


	return 0;
}
