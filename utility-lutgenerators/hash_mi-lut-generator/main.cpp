#include <iostream>
#include <fstream>

const int max_size = 24;

int main(int argc, char const *argv[]) {

	std::ofstream stream;
	stream.open("hash_mi.h");

	stream << "switch(size) {\n";
	for (int i = 0; i < max_size; ++i) {
		stream << "    case " << i << ": ";
		for(int l = 0; l < i + 1; ++l) {
			stream << " result += (s[" << l << "] * primes[" << l << "]); ";
		}
		stream << "break;" << std::endl;
	}
	stream << "    default: return 0; break;\n}";

	stream.close();
	
	return 0;
}
