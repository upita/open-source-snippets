#ifndef _STRINGUTILITY_H
#define _STRINGUTILITY_H
#include <stdlib.h>
#include <cstdio>
#include <iostream>

inline unsigned int size(const char* s) {
	int i = 0;
	while(s[i] != 0) i++;
	return i;
}

inline unsigned int size(char* c) {
	int i = 0;
	while(c[i] != 0) i++;
	return i;
}

inline bool isLiteral(char c) {
	return (c >= 65 && c <= 90) || (c >= 97 && c <= 122);
}

inline bool isNumeral(char c) {
	return c >= 48 && c <= 57;
}

inline bool isSpecial(char c) {
	return (c <= 32 || c == 127) && c != 32;
}

inline bool isPrintable(char c) {
	return !isSpecial(c);
}

inline char toUpperCase(char c) {
	if(c >= 97 && c <= 122) {
		c = c - 32;
	}
	return c;
}

inline char* toUpperCase(const char* s, unsigned int size) {

	char* result = (char*) malloc(sizeof(char) * size);

	for (int i = 0; i < size; ++i)
		result[i] = toUpperCase(s[i]);

	return result;
}

inline char* toUpperCase(const char* s) {
	return toUpperCase(s, size(s));
}

inline char toLowerCase(char s) {
	if(s >= 65 && s <= 90)
		s += 32;
	return s;
}

inline char* toLowerCase(const char* s, unsigned int size) {

	char* result = (char*) malloc(sizeof(char) * size); //NOTE: char size is always 1 byte

	for (int i = 0; i < size; ++i)
		result[i] = toLowerCase(s[i]);

	return result;
}

inline char* toLowerCase(const char* s) {
	return toLowerCase(s, size(s));
}

#endif
