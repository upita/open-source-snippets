#ifndef _DOUBLEBUFFER_H
#define _DOUBLEBUFFER_H

/**
*Double buffering implementation with two different buffers.
*/
template<typename T>
class DoubleBuffer {
	private:
		T buffers[2];
		bool first = true;
	public:

		inline DoubleBuffer() {}
		
		inline ~DoubleBuffer() {}

		inline T& get() {
			if(first) return buffers[0];
			return buffers[1];
		}

		inline T& next() {
			if(first) return buffers[1];
			return buffers[0];
		}

		inline void swap() {
			first = !first;
		}

		inline T& operator[](int i) {
			if(i == 0) return get();
			else if(i == 1) return next();
		}

};

#endif
