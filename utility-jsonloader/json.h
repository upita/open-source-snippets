#ifndef _JSON_H
#define _JSON_H
#include "loadedjson.h"

/**
*@brief Loads the specified JSON file into a LoadedJSON structure.
*@param filename The JSON filename's.
*@return LoadedJSON The structure containing the data loaded from file.
*/
LoadedJSON load(std::string filename);

#endif
