### JSONLoader
JSONLoader is a simple JSON loader for videogames and applications which don't need support for arrays and sub-objects.

### TO-DO
* support for arrays
* support for sub-objects
* optimization
* malformed JSON errors
