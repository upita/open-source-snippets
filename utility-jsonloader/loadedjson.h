#ifndef _LOADEDJSON_H
#define _LOADEDJSON_H
#include "object.h"
#include <map>

class LoadedJSON {
	public:
		int error;
		std::map<std::string, Object> objects;

};

#endif
