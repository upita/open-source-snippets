#include "json.h"
#include <iostream>
#include <fstream>
#include <cstdlib>

#define RETURN_ERROR LoadedJSON errorJSON;errorJSON.error = -2;return errorJSON;

enum JSONParserState {
	UNSPECIFIED,
	OBJECT_NAME,
	OBJECT_INSIDE_WAIT,
	OBJECT_INSIDE,
	LABEL_NAME,
	LABEL_VALUE_WAIT,
	LABEL_VALUE
};

inline bool isLiteral(char c) {
	return (c >= 65 && c <= 90) || (c >= 97 && c <= 122);
}

inline bool isNumeral(char c) {
	return c >= 48 && c <= 57;
}

LoadedJSON load(std::string filename) {

	std::ifstream stream;
	stream.open(filename);

	std::string buffer = "";

	if(stream.is_open()) {

		std::string line;

		while(std::getline(stream, line)) {
			buffer += line;
		}

	} else {
		LoadedJSON errorJSON;
		errorJSON.error = -1;
		return errorJSON;
	}

	LoadedJSON result = LoadedJSON();

	JSONParserState state = UNSPECIFIED;

	std::string value = "";
	std::string labelName = "";
	std::string objectName = "";
	bool closedValue = false;
	bool insideObject = false;
	LabelDataType type = NUMBER;


	for(size_t i = 0; i < buffer.size(); ++i) {

		char c = buffer.at(i);

		switch((short) state) {

				case UNSPECIFIED: {

					if(c == '\"' || c == '\'') {
						objectName = "";
						state = OBJECT_NAME;
					}

					break;
				}
				case OBJECT_NAME: {

					if(isLiteral(c) || isNumeral(c)) {
						objectName += c;
					} else if(c == '\"' || c == '\'') {
						state = OBJECT_INSIDE_WAIT;
						Object o = Object();
						o.name = objectName;
						result.objects.insert(std::pair<std::string, Object>(o.name, o));
					}

					break;
				}
				case OBJECT_INSIDE_WAIT: {

					if(c == '{') {
						state = OBJECT_INSIDE;
						insideObject = true;
					}

					break;
				}
				case OBJECT_INSIDE: {

					if(c == '\"' || c == '\'') {
						state = LABEL_NAME;
					} else if(c == '}') {
						state = UNSPECIFIED;
					}

					break;
				}
				case LABEL_NAME: {

					if(isLiteral(c) || isNumeral(c)) {
						labelName += c;
					} else if(c == '\"' || c == '\'') {
						state = LABEL_VALUE_WAIT;
					}

					break;
				}
				case LABEL_VALUE_WAIT: {

					if(c == '\"' || c == '\'') {
						state = LABEL_VALUE;
						type = STRING;
						value = "";
					} else if(isNumeral(c)) {
						state = LABEL_VALUE;
						type = NUMBER;
						value += c;
					}

					break;
				}
				case LABEL_VALUE: {

					if(isNumeral(c) || isLiteral(c)) {
						value += c;
					} else if((c == '\'' || c == '\"') && type == STRING) {
						state = OBJECT_INSIDE;

						Label l = Label();
						l.type = STRING;
						l.name = labelName;
						l.string_value = value;

						result.objects[objectName].labels.insert(std::pair<std::string, Label>(l.name, l));

						value = "";
						labelName = "";
						closedValue = false;
						insideObject = false;
						type = NUMBER;

					} else if((c == ',' || c == '}' || c == '	') && type == NUMBER) {
						state = OBJECT_INSIDE;

						Label l = Label();
						l.type = NUMBER;
						l.name = labelName;
						l.number_value = std::atof(value.c_str());
						l.string_value = value;

						result.objects[objectName].labels.insert(std::pair<std::string, Label>(l.name, l));

						value = "";
						labelName = "";
						closedValue = false;
						insideObject = false;
						type = NUMBER;
					}

					break;
				}

		}

	}


	return result;
}
