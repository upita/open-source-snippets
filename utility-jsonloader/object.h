#ifndef _OBJECT_H
#define _OBJECT_H
#include "label.h"
#include <map>

class Object {
	public:
		std::string name;
		std::map<std::string, Label> labels;
	
};

#endif
