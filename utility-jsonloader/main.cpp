#include "json.h"
#include "loadedjson.h"
#include <iostream>

int main(int argc, char const *argv[]) {
	
	LoadedJSON myjson = load("example.json");

	if(myjson.error < 0) {
		std::cout << "ERROR" << std::endl;
	}
	std::cout << "This boss is badass!" << std::endl;
	std::cout << "His health is " <<
	myjson.objects["Boss"].labels["health"].string_value << "!" << std::endl;

	return 0;
}
