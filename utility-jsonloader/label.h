#ifndef _JSONNODE_H
#define _JSONNODE_H
#include <string>
#include <vector>


enum LabelDataType {
	NUMBER,
	STRING
};

class Label {
	public:

		LabelDataType type;

		std::string name;

		float number_value = 0;
		std::string string_value = "";

		Label() {}

		Label(LabelDataType type) : type(type) {}

};

#endif
