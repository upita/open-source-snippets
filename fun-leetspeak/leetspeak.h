#ifndef _LEETSPEAK_H
#define _LEETSPEAK_H
#include <string>
#include <ctime>
#include <iostream>
#include <cstring>

//1f y0u c4n r34d th1s y0u 1s r34lly h4x0r!!!1!1!11
inline std::string l33tify(std::string s) {
	const size_t size = s.size();
	for(size_t i = 0; i < size; ++i) {
		char c = s.at(i);
		switch(c) {
			case 'i': s[i] = '1'; break;
			case 's': s[i] = '5'; break;
			case 'a': s[i] = '4'; break;
			case 'o': s[i] = '0'; break;
			case 't': s[i] = '7'; break;
			case 'e': s[i] = '3'; break;
			case 'I': s[i] = '1'; break;
			case 'S': s[i] = '5'; break;
			case 'A': s[i] = '4'; break;
			case 'O': s[i] = '0'; break;
			case 'T': s[i] = '7'; break;
			case 'E': s[i] = '3'; break;
		}
	}
	return s;
}

#endif
