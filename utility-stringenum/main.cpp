#include <iostream>
#include "stringenum.h"

int main(int argc, char const *argv[]) {
	
	auto h = senum_encode("hello");

	std::cout << senum_decode(h) << std::endl;

	return 0;
}
