#ifndef _STRINGENUM_H
#define _STRINGENUM_H
#include <iostream>
#include <string>
#include <cmath> //temporary

inline unsigned long long int senum_encode(std::string s) {
	const size_t size = s.size();

	register unsigned long long int res;

	for(int i = 0; i < size; ++i)
		res += s[i] * (i * 100);
	return res;
}

inline std::string senum_decode(unsigned long long int hash) {

	std::string res = "";

	size_t size = 0;

	register const float hash_f = (float) hash;

	while(hash > std::pow(100, size)) size++;
	
	for(int i = 0; i < size; ++i)
		res += char(hash / std::pow(100, i));

	return res;
}

#endif
