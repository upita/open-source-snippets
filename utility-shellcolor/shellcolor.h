#ifndef _SHELLCOLOR_H
#define _SHELLCOLOR_H
#include <cstdlib>
#include <string>
#include <iostream>
#include <sstream>

typedef enum {
	SHELL_BLACK = 30,
	SHELL_RED = 31,
	SHELL_GREEN = 32,
	SHELL_YELLOW = 33,
	SHELL_BLUE = 34,
	SHELL_MAGENTA = 35,
	SHELL_CYAN = 36,
	SHELL_WHITE = 37,
	SHELL_RESET = 0
} SHELL_COLOR;

inline void set_shell_color(SHELL_COLOR color) {
	std::stringstream stream;
	stream << (int) color;
	std::cout << std::string("\e[" + stream.str() + "m").c_str();
}

#endif
