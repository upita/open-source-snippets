#ifndef _PRINTUTILITY_H
#define _PRINTUTILITY_H
#include <iostream>

//Prints any data type with << operator overload
//using variadic templates
inline void print() {}
template<typename first, typename ... many>
inline void print(first arg, const many&... rest) {
	std::cout << arg;
	print(rest...);
}

//Prints any data type with << operator overload and prints a newline
//using variadic templates
inline void println() {
	std::cout << std::endl;
}
template<typename first, typename ... many>
inline void println(first arg, const many&... rest) {
	std::cout << arg;
	println(rest...);
}

#endif
