#ifndef _ARGLIST_H
#define _ARGLIST_H
#include <string>
#include <vector>

#ifndef _STRINGUTILITY_H
inline bool isQuotes(char c) {
	return c == '\'' || c == '\"';
}
#endif

class ArgList {
	private:
		std::vector<std::string> list;
	public:
		// Initialize the argument list.
		inline ArgList(int argc, char const *argv[]) {

			std::string s;
			for (int i = 0; i < argc; ++i)
				s += std::string(argv[i]) + ' ';

			for (int i = 0; i < s.size(); ++i) {
				std::string curr;

				if(s[i] == '\n' && s[i] == '\0') break;
				
				if(isQuotes(s[i])) {
					i++;
					while(!isQuotes(s[i]) && s[i] != '\n' && s[i] != '\0') {
						curr += s[i];
						i++;
					}
				} else {
					while(!isQuotes(s[i]) && s[i] != ' ' && s[i] != '\n' && s[i] != '\0') {
						curr += s[i];
						i++;
					}
				}
				list.push_back(curr);
			}
		}

		// Get the nth parameter
		inline std::string operator[](size_t i) {
			if(list.size() > i) return list[i];
			return "";
		}

		// Check if the arguments contain the given string.
		inline bool contains(std::string s) {
			for (int i = 0; i < list.size(); ++i)
				if(list[i] == s) return true;
			return false;
		}

		// Check if the arguments contain the given string or its first 2 characters.
		inline bool contains_s(std::string s) {
			for (int i = 0; i < list.size(); ++i)
				if(list[i] == s) return true;
			std::string s2; s2 += s[0]; s2 += s[1];
			for (int i = 0; i < list.size(); ++i)
				if(list[i] == s2) return true;
			return false;
		}

		// Get the number of arguments.
		inline size_t size() {
			return list.size();
		}
		
};

#endif
