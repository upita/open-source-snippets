### open-source-snippets
This repo contains a collection of snippets used into the development of our products.
You can find C++ utility programs and Python scripts.

### How to use
In the folders you can find the implementation files or scripts.
There are no dependencies otherwise stated differently.

### License
You can use the snippets provided under the MIT License.
If you happen to use something in commercial or open source projects we'd like to know it,
you can drop us an email at [info@upitasoft.com](mailto:info@upitasoft.com).